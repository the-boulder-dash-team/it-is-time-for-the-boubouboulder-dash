package utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

/**
 * A class that is used to load image.
 */

public class ImageLoader {

    /**
     * Method that loads in our images
     *
     * @param path of the image with all the sprite inside
     * @return null if this is a error
     */

    public static BufferedImage loadImage(String path) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try {
            return ImageIO.read(classLoader.getResource(path));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
