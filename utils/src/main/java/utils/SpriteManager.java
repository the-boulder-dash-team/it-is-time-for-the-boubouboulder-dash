package utils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;

/**
 * A class that is used to load the different sprites of the game.
 */

public class SpriteManager {

    private static SpriteManager instance = null;
    private final int width = 16, height = 16;
    private HashMap<String, Image> playerSprites;
    private HashMap<String, Image> enemySprites;
    private Image boulderSprite, diamondSprites, dirtSprite, breakableWallSprite, boundingWallSprite, endBlockSprite, backgroundSprite;

    /**
     * Constructor of a position.
     */

    private SpriteManager() {
        this.playerSprites = new HashMap<>();
        this.enemySprites = new HashMap<>();
        loadPlayerSprites();
        loadBackgroundSprites();
        loadBoulderSprites();
        loadBoundingWallSprites();
        loadBreakableWallSprites();
        loadDiamondSprites();
        loadDirtWallSprites();
        loadEnemySprites();
        loadEndBlockSprites();
    }

    /**
     * Implement some logic to be sure there is only one instance of a class.
     *
     * @return the instance
     */

    public static SpriteManager getInstance() {

        if (instance == null) {
            instance = new SpriteManager();
        }
        return instance;
    }

    /**
     * Create a unique instance which is accessed by the class
     */

    public static void createInstance() {
        instance = new SpriteManager();
    }

    /**
     * Allows to take a part of an image and to keep the one that interests us from the .png with a position and dimension
     */

    private void loadPlayerSprites() {
        BufferedImage playerSpritesheet = ImageLoader.loadImage("player.png");
        if (playerSpritesheet != null) {
            playerSprites.put("down", playerSpritesheet.getSubimage(0 * 16, 12 * 16, 16, 16));
            playerSprites.put("left", playerSpritesheet.getSubimage(1 * 16, 1 * 16, 16, 16));
            playerSprites.put("right", playerSpritesheet.getSubimage(1 * 16, 3 * 16, 16, 16));
            playerSprites.put("up", playerSpritesheet.getSubimage(2 * 16, 5 * 16, 16, 16));

        }
    }

    /**
     * Allows to take a part of an image and to keep the one that interests us from the .png with a position and dimension
     */

    private void loadEnemySprites() {
        BufferedImage enemySpritesheet = ImageLoader.loadImage("tileset.png");
        enemySprites.put("enemy", enemySpritesheet.getSubimage(9 * 16, 0 * 16, 16, 16));

    }

    /**
     * Allows to take a part of an image and to keep the one that interests us from the .png with a position and dimension
     */

    private void loadBoundingWallSprites() {
        BufferedImage boundingWallSpritesheet = ImageLoader.loadImage("tileset.png");
        boundingWallSprite = boundingWallSpritesheet.getSubimage(0, 0, 16, 16);

    }

    /**
     * Allows to take a part of an image and to keep the one that interests us from the .png with a position and dimension
     */

    private void loadBreakableWallSprites() {
        BufferedImage breakableWallSpritesheet = ImageLoader.loadImage("tileset.png");
        breakableWallSprite = breakableWallSpritesheet.getSubimage(0, 0, 16, 16);

    }

    /**
     * Allows to take a part of an image and to keep the one that interests us from the .png with a position and dimension
     */

    private void loadDirtWallSprites() {
        BufferedImage dirtSpritesheet = ImageLoader.loadImage("tileset.png");
        dirtSprite = dirtSpritesheet.getSubimage(1 * 16, 0, 16, 16);

    }

    /**
     * Allows to take a part of an image and to keep the one that interests us from the .png with a position and dimension
     */

    private void loadDiamondSprites() {
        BufferedImage diamondSpritesheet = ImageLoader.loadImage("tileset.png");
        diamondSprites = diamondSpritesheet.getSubimage(4 * 16, 0, 16, 16);

    }

    /**
     * Allows to take a part of an image and to keep the one that interests us from the .png with a position and dimension
     */

    private void loadBoulderSprites() {
        BufferedImage boulderSpritesheet = ImageLoader.loadImage("tileset.png");
        boulderSprite = boulderSpritesheet.getSubimage(3 * 16, 0, 16, 16);

    }

    /**
     * Allows to take a part of an image and to keep the one that interests us from the .png with a position and dimension
     */

    private void loadEndBlockSprites() {
        BufferedImage endBlockSpritesheet = ImageLoader.loadImage("tileset.png");
        endBlockSprite = endBlockSpritesheet.getSubimage(6 * 16, 0, 16, 16);
    }

    /**
     * Allows to take a part of an image and to keep the one that interests us from the .png with a position and dimension
     */

    private void loadBackgroundSprites() {
        BufferedImage background = ImageLoader.loadImage("tileset.png");
        backgroundSprite = background.getSubimage(2 * 16, 0, 16, 16);
    }

    /**
     *
     * @return the list of player sprite
     */

    public HashMap<String, Image> getPlayerSprites() {
        return playerSprites;
    }

    /**
     * Update the player sprite
     *
     * @param playerSprites
     */

    public void setPlayerSprites(HashMap<String, Image> playerSprites) {
        this.playerSprites = playerSprites;
    }

    /**
     *
     * @return the list of enemy sprite
     */

    public HashMap<String, Image> getEnemySprites() {
        return enemySprites;
    }

    /**
     * Update the enemy sprite
     *
     * @param enemySprites
     */

    public void setEnemySprites(HashMap<String, Image> enemySprites) {
        this.enemySprites = enemySprites;
    }

    /**
     *
     * @return the image of the boulder
     */

    public Image getBoulderSprite() {
        return boulderSprite;
    }

    /**
     * Update the boulder sprite
     *
     * @param boulderSprite
     */

    public void setBoulderSprite(Image boulderSprite) {
        this.boulderSprite = boulderSprite;
    }

    /**
     *
     * @return the image of the diamond
     */

    public Image getDiamondSprite() {
        return diamondSprites;
    }

    /**
     * Update the diamond sprite
     *
     * @param diamondSprites
     */

    public void setDiamondSprites(Image diamondSprites) {
        this.diamondSprites = diamondSprites;
    }

    /**
     *
     * @return the image of the dirt
     */

    public Image getDirtSprite() {
        return dirtSprite;
    }

    /**
     * Update the dirt sprite
     *
     * @param dirtSprite
     */

    public void setDirtSprite(Image dirtSprite) {
        this.dirtSprite = dirtSprite;
    }

    /**
     *
     * @return the image of the wall
     */

    public Image getBreakableWallSprite() {
        return breakableWallSprite;
    }

    /**
     * Update the wall sprite
     *
     * @param breakableWallSprite
     */

    public void setBreakableWallSprite(Image breakableWallSprite) {
        this.breakableWallSprite = breakableWallSprite;
    }

    /**
     *
     * @return the image the wall
     */

    public Image getBoundingWallSprite() {
        return boundingWallSprite;
    }

    /**
     * Update the wall sprite
     *
     * @param boundingWallSprite
     */

    public void setBoundingWallSprite(Image boundingWallSprite) {
        this.boundingWallSprite = boundingWallSprite;
    }

    /**
     *
     * @return the image of the endblock sprite
     */

    public Image getEndBlockSprite() {
        return endBlockSprite;
    }

    /**
     * Update the endblock sprite
     *
     * @param endBlockSprite
     */

    public void setEndBlockSprite(Image endBlockSprite) {
        this.endBlockSprite = endBlockSprite;
    }

    /**
     *
     * @return the image of the background
     */

    public Image getBackgroundSprite() {
        return backgroundSprite;
    }

    /**
     * Update the background sprite
     *
     * @param backgroundSprite
     */

    public void setBackgroundSprite(Image backgroundSprite) {
        this.backgroundSprite = backgroundSprite;
    }
}
