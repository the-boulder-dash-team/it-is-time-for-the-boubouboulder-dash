package utils;

/**
 * A class the provides methods for multiple other classes (shared code)
 */

public class Utils {

    /**
     *
     * @param t
     * @param o
     * @param <T>
     * @return
     */
    public static <T> T as(Class<T> t, Object o){
        if(t.isInstance(o)){
            return t.cast(o);
        }
        return null;
    }
}
