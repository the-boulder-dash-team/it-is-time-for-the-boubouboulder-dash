package main;

import contract.IMap;
import controller.Controller;
import controller.GameEngine;
import model.dao.DAOFactory;
import view.View;

public abstract class Main {

    public static void main(final String[] args) {


        IMap map = DAOFactory.getInstance().getMapDAO().getMap(1);
        Controller controller = new Controller(map.getPlayer());
        GameEngine game = new GameEngine(map, controller);
        View view = new View(map, game);
        view.setController(controller);
        view.setGameController(game);

        game.runGameLoop();
    }
}

