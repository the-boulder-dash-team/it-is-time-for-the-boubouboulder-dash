package contract;

import java.awt.*;

public interface IEntity extends ICollideable, IDestroyable{
    IPosition getPosition();

    void setPosition(IPosition position);

    Image getSprite();

    IMap getMap();

    boolean isPlayer();

    void setMap(IMap IMap);

    void setSprite(Image sprite);

    boolean isDrawable();

    void setDrawable(boolean bool);

}
