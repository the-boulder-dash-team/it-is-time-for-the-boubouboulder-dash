package contract;

import enums.Direction;

public interface IMobile {
    boolean canMove(IPosition futurePosition);

    boolean move(Direction direction);

    void move();
}
