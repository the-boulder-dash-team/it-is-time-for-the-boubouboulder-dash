package contract;

public interface IPosition extends Cloneable {
    int getX();

    void setX(int x);

    int getY();

    void setY(int y);

    int getGridCellX();

    int getGridCellY();

    void setGridCellX(int x);

    void setGridCellY(int y);

    IPosition clone();
}
