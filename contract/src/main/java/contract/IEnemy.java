package contract;

public interface IEnemy extends IEntity, ICollideable, IDestroyable, IMobile {
}
