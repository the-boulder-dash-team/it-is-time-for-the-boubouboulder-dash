package contract;

public interface IDestroyable {

    void destroy();

}
