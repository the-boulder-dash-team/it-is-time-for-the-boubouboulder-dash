package contract;

public interface ICollideable {

    boolean isColliding(IEntity entity, IPosition position);
}
