package contract;

public interface IController {
    void orderPerform(int keycode);
    IPlayer getPlayer();
}
