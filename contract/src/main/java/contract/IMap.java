package contract;

import javax.swing.*;
import java.util.List;

public interface IMap {

    List<IEntity> getEntities();

    void setEntities(List<IEntity> entities);

    Timer getTime();

    void setTime(Timer time);

    int getDiamondsCounter();

    void setDiamondsCounter(int diamondsCounter);

    IPlayer getPlayer();

    void setPlayer(IPlayer player);

    void decrementDiamondsCounter();

    boolean isCleaned();

    void setCleaned(boolean bool);
}
