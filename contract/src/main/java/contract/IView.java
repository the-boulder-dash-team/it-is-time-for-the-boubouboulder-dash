package contract;

public interface IView {

    void show();

    void hide();
}
