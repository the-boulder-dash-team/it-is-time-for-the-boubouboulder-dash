package contract;

public interface IGravityable {

    boolean canRoll();

    void roll();

    boolean isRolling();

    boolean canFall();

    void fall();

    boolean isFalling();

}
