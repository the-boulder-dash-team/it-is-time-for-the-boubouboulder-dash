package contract;

import java.awt.event.KeyEvent;
import java.util.Observable;

public interface IGameController {
    void handleKeyEvent(KeyEvent e);
    Observable getObservable();
}
