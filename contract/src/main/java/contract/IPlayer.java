package contract;
import java.awt.*;

public interface IPlayer extends IMobile{

    Image getSprite();

    IPosition getPosition();

    boolean isAlive();

}
