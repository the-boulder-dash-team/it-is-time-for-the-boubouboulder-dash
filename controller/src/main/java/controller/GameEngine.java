package controller;

import contract.*;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import static java.awt.event.KeyEvent.VK_P;
import static utils.Utils.as;

public class GameEngine extends Observable implements IGameController {

    private IView view;
    private IController controller;
    private IMap map;
    private boolean paused = true;
    private int keyPressed = 0;

    /**
     * @param map        Game's map.
     * @param controller Controller for the player.
     */
    public GameEngine(IMap map, IController controller) {
        this.map = map;
        this.controller = controller;
    }

    /**
     * Recovers to ViewFrame the type of pressure for a key
     *
     * @param e The type of pressure for a key.
     */
    public void handleKeyEvent(KeyEvent e) {
        switch (e.getID()) {
            case KeyEvent.KEY_PRESSED:
                keyPressed = e.getKeyCode();
                if (e.getKeyCode() == VK_P)
                    paused = !paused;
                e.consume();
                break;
            case KeyEvent.KEY_RELEASED:
                if(keyPressed == e.getKeyCode()) {
                    keyPressed = 0;
                }
                e.consume();
                break;
            default:
                e.consume();
                break;

        }
    }

    @Override
    public Observable getObservable() {
        return this;
    }

    public void runGameLoop() {
        Thread loop = new Thread(this::gameLoop);
        loop.start();
    }

    /**
     * Makes loop the game.
     */
    private void gameLoop() {
        final double updateFrequency = 100000000;
        double lastUpdateTime = System.nanoTime();

        while (map.getPlayer().isAlive() && !map.isCleaned()) {
            double now = System.nanoTime();
            if (!paused) {
                while (now - lastUpdateTime > updateFrequency) {
                    updateGame();
                    setChanged();
                    lastUpdateTime += updateFrequency;
                }

                notifyObservers();
                //Yield until it has been at least the target time between renders. This saves the CPU from hogging.
                while (now - lastUpdateTime < updateFrequency) {
                    now = System.nanoTime();
                }
            } else {
                while (now - lastUpdateTime > updateFrequency) {
                    lastUpdateTime += updateFrequency;
                }
            }
        }

        if (!map.getPlayer().isAlive()) {
            System.out.println("You have died...");
            setChanged();
            notifyObservers(true);
        }
        if (map.isCleaned()) {
            System.out.println("You won");
            setChanged();
            notifyObservers(false);
        }
        while (true) {
            controller.orderPerform(keyPressed);
        }
    }

    /**
     * Updates the list of entities.
     * Make move the enemies.
     * Make roll and fall the gravityable entities.
     */
    private void updateGame() {
        controller.orderPerform(keyPressed);

        List<IEntity> entities = new ArrayList<>(map.getEntities());

        for (IEntity entity :
                entities) {
            IGravityable gravitied = as(IGravityable.class, entity);
            if (gravitied != null) {
                gravitied.fall();
                gravitied.roll();
            }
            IEnemy enemy = as(IEnemy.class, entity);
            if (enemy != null) {
                enemy.move();
            }
        }
    }

    /**
     * @return Returns View.
     */
    public IView getView() {
        return view;
    }

    /**
     * @param view Sets View.
     */
    public void setView(IView view) {
        this.view = view;
    }
}
