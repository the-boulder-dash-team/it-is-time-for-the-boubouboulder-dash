package controller;

import contract.IController;
import contract.IPlayer;
import enums.Direction;

import java.awt.event.KeyEvent;

public class Controller implements IController {

    private IPlayer player;

    /**
     *
     * @param player Game's player.
     */
    public Controller(IPlayer player) {
        this.setPlayer(player);
    }

    /**
     * Calls the correct method in function of the key pressed.
     * @param keycode The key code of the key pressed.
     */
    public void orderPerform(int keycode) {

        switch (keycode) {
            case KeyEvent.VK_ESCAPE:
                System.exit(0);
                break;
            case KeyEvent.VK_UP:
                player.move(Direction.UP);
                break;
            case KeyEvent.VK_DOWN:
                player.move(Direction.DOWN);
                break;
            case KeyEvent.VK_LEFT:
                player.move(Direction.LEFT);
                break;
            case KeyEvent.VK_RIGHT:
                player.move(Direction.RIGHT);
                break;
        }
    }

    /**
     *
     * @return Returns the player.
     */
    public IPlayer getPlayer() {
        return player;
    }

    /**
     *
     * @param player Sets the player.
     */
    public void setPlayer(IPlayer player) {
        this.player = player;
    }
}