package model.position;

import model.world.Position;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PositionTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void getX() {

        final int expected = 64;

        final Position position =new Position(2,1);

        assertEquals(expected, position.getX());

    }

    @Test
    void setX() {

        final int expected = 2;

        final Position position =new Position(0,1);

        position.setX(2);

        assertEquals(expected, position.getX());


    }

    @Test
    void getY() {

        final int expected = 64;

        final Position position =new Position(0,2);

        assertEquals(expected, position.getY());

    }

    @Test
    void setY() {

        final int expected = 2;

        final Position position =new Position(0,0);

        position.setY(2);

        assertEquals(expected, position.getY());

    }


    @Test
    void getGridCellX() {
        final int expected = 56;

        final Position position = new Position(56,1);

        assertEquals(expected, position.getGridCellX());
    }

    @Test
    void getGridCellY() {

        final int expected = 45;

        final Position position = new Position(0,45);

        assertEquals(expected, position.getGridCellY());

    }
}