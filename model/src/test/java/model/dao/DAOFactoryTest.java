package model.dao;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class DAOFactoryTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getInstance() {
        assertNotEquals(DAOFactory.getInstance(), null);
    }

    @Test
    void getConnection() throws SQLException {
        assertTrue(DAOFactory.getInstance().getConnection().isValid(0));
        DAOFactory.getInstance().getEntityDAO().getMapEntities(1);
    }
}