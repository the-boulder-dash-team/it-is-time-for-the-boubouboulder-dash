package model.mobile;

import contract.IEntity;
import enums.Direction;
import contract.IMap;
import model.world.Map;
import model.world.Position;
import model.entity.Player;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;


import java.awt.*;
import java.awt.image.BufferedImage;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MobileTest {

    @Test
    void move() {

        final int expected = 2;

        final Position position = new Position(3, 2);

        Image image = new BufferedImage(32, 32, BufferedImage.TYPE_INT_RGB);

        ArrayList<IEntity> alist=new ArrayList<IEntity>();

        int counterDiamond = 2;

        IMap IMap = new Map(alist, null,2);

        Player player = new Player(position, true, image, IMap);

        player.move(Direction.LEFT);

        assertEquals(expected, position.getGridCellX());


    }
}