package model.entity;

/**
 * ENUM TypeEntity
 */
public enum TypeEntity {
    BreakableWally,
    BoundingWally,
    Dirty,
    Backgroundy,
    Bouldery,
    Diamondy,
    EndBlocky,
    Butterflyy,
    Playery,
    FasteryEnemyy,
}
