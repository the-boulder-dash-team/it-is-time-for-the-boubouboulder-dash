package model.entity;

import contract.*;
import model.world.Position;

import java.awt.*;

import static utils.Utils.as;

public class Player extends Mobile implements IPlayer, IDestroyable {

    private boolean isAlive;

    /**
     *
     * @param position His position in the map.
     * @param isAlive His status dead or alive.
     * @param sprite His sprite for the game.
     * @param IMap His map.
     */
    public Player(Position position, boolean isAlive, Image sprite, IMap IMap) {
        super(position, sprite, IMap);
        this.isAlive = isAlive;
    }

    /**
     *
     * @return Returns true beacause it is the player.
     */
    @Override
    public boolean isPlayer() {
        return true;
    }

    /**
     *
     * @return Returns isAlive attribute.
     */
    public boolean isAlive() {
        return isAlive;
    }

    /**
     *
     * @param alive Sets isAlive attribute.
     */
    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    /**
     * Deletes this player.
     */
    @Override
    public void destroy() {
        this.setAlive(false);
        this.setDrawable(false);
    }

    /**
     * Checks whether this player and the entity passed to the method will collide.
     * @param entity The potentially colliding entity.
     * @param futurePosition Future position of the player.
     * @return Returns true if the collision prevents movement.
     */
    @Override
    public boolean isColliding(IEntity entity, IPosition futurePosition) {
        if (this.getPosition().equals(futurePosition)) {
            if (as(Boulder.class, entity) != null || as(Diamond.class, entity) != null) {
                if (((Gravityable) entity).isFalling() || ((Gravityable) entity).isRolling()) {
                    this.destroy();
                    return false;
                }
            } else if (as(Enemy.class, entity) != null) {
                this.destroy();
                return false;
            }
            return true;
        }
        return false;
    }

}
