package model.entity;

import contract.IEntity;
import contract.IMap;
import contract.IPosition;
import model.world.Position;

import java.awt.*;

import static utils.Utils.as;

public class EndBlock extends Entity {

    /**
     *
     * @param position Its position in the map.
     * @param sprite Its sprite for the game.
     * @param IMap Its map.
     */
    public EndBlock(Position position, Image sprite, IMap IMap) {
        super(position, sprite, IMap);
    }

    /**
     * Checks whether this end block and the entity passed to the method will collide and checks whether counter of diamonds is equal to 0.
     * @param entity The potentially colliding entity.
     * @param futurePosition Future position of the entity.
     * @return Returns true if the collision prevents movement.
     */
    @Override
    public boolean isColliding(IEntity entity, IPosition futurePosition) {
        if (this.getPosition().equals(futurePosition)) {
            if (as(Player.class, entity) != null) {
                if (getMap().getDiamondsCounter() == 0)
                    getMap().setCleaned(true);
            }
        }
        return false;
    }

}
