package model.entity;

import contract.IMap;
import model.world.Position;

import java.awt.*;

public abstract class AbstractWall extends Entity {

    /**
     *
     * @param position Its position in the map.
     * @param sprite Its sprite for the game.
     * @param IMap Its map.
     */
    public AbstractWall(Position position, Image sprite, IMap IMap) {
        super(position, sprite, IMap);
    }
}

