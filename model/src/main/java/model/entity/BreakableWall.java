package model.entity;

import contract.IDestroyable;
import contract.IMap;
import model.world.Position;

import java.awt.*;

public class BreakableWall extends AbstractWall implements IDestroyable {

    /**
     *
     * @param position Its position in the map.
     * @param sprite Its sprite for the game.
     * @param IMap Its map.
     */
    public BreakableWall(Position position, Image sprite, IMap IMap) {
        super(position, sprite, IMap);
    }

    /**
     * Deletes this breakable wall.
     */
    @Override
    public void destroy() {
        getMap().getEntities().remove(this);
    }

}
