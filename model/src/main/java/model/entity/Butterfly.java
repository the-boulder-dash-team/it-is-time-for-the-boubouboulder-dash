package model.entity;

import model.world.Position;

import java.awt.*;

public class Butterfly extends Enemy{

    /**
     *
     * @param position Its position in the map.
     * @param sprite Its sprite for the game.
     * @param IMap Its map.
     */
    public Butterfly(Position position, Image sprite, contract.IMap IMap) {
        super(position, sprite, IMap);
    }

    /**
     * Deletes this enemy and explodes him and replaces entities destroyed by him with diamonds.
     */
    @Override
    protected void explosion() {
        super.explosion();
        putDiamonds();
    }
}
