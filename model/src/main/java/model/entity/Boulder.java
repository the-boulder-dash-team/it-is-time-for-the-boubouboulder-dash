package model.entity;

import contract.IDestroyable;
import contract.IEntity;
import contract.IPosition;
import enums.Direction;
import model.world.Position;

import java.awt.*;
import java.util.List;

import static utils.Utils.as;

public class Boulder extends Gravityable implements IDestroyable {
    private boolean leftBlocked;
    private boolean rightBlocked;

    /**
     *
     * @param position Its position in the map.
     * @param sprite Its sprite for the game.
     * @param IMap Its map.
     */
    public Boulder(Position position, Image sprite, contract.IMap IMap) {
        super(position, sprite, IMap);
    }

    /**
     * Deletes this boulder.
     */

    public boolean canBePushed(){
        leftBlocked = false;
        rightBlocked = false;
        List<IEntity> entities = this.getMap().getEntities();

        for (IEntity entity :
                entities) {
            if(!leftBlocked)
            leftBlocked = hasOnLeft(entity);
            if(!rightBlocked)
                rightBlocked = hasOnRight(entity);
        }


        return !(leftBlocked && rightBlocked);
    }

    public void push(Direction direction){
        if(canBePushed()){
            move(direction);
        }
    }

    @Override
    public void destroy() {
        getMap().getEntities().remove(this);
    }

    /**
     * Checks whether this player and the entity passed to the method will collide.
     * @param entity The potentially colliding entity.
     * @param futurePosition Future position of the player.
     * @return Returns true if the collision prevents movement.
     */
    @Override
    public boolean isColliding(IEntity entity, IPosition futurePosition) {
        int xMove = entity.getPosition().getGridCellX() - futurePosition.getGridCellX();
        if (this.getPosition().equals(futurePosition)) {
            if (as(Player.class, entity) != null && xMove != 0 && canBePushed()) {
                if (xMove > 0 && !leftBlocked) {
                    this.move(Direction.LEFT);
                } else if (xMove < 0 &&!rightBlocked) {
                    this.move(Direction.RIGHT);
                }
                else{
                    return true;
                }
                return false;
            }
            return true;
        }
        return false;
    }


}
