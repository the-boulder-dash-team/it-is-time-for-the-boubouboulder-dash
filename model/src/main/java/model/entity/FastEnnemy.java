package model.entity;

import contract.IMap;
import model.world.Position;

import java.awt.*;

public class FastEnnemy extends Enemy{

    /**
     * @param position Its position in the map.
     * @param sprite   Its sprite for the game.
     * @param IMap     Its map.
     */
    public FastEnnemy(Position position, Image sprite, contract.IMap IMap) {
        super(position, sprite, IMap);
    }

    @Override
    public void move() {
        super.move();
        super.move();
    }
}
