package model.entity;

import contract.IMap;
import model.world.Position;

import java.awt.*;

public class BoundingWall extends AbstractWall {

    /**
     *
     * @param position Its position in the map.
     * @param sprite Its sprite for the game.
     * @param IMap Its map.
     */
    public BoundingWall(Position position, Image sprite, IMap IMap) {
        super(position, sprite, IMap);
    }

    @Override
    public void destroy() {
    }
}
