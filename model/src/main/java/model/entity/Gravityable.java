package model.entity;

import contract.IEntity;
import contract.IGravityable;
import contract.IPosition;
import model.world.Position;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static utils.Utils.as;

public abstract class Gravityable extends Mobile implements IGravityable {
    protected boolean falling = true;
    protected boolean rolling = true;
    private boolean rollLeft;
    private boolean rollRight;

    /**
     * @param position Its position in the map.
     * @param sprite   Its sprite for the game.
     * @param IMap     Its map.
     */
    public Gravityable(Position position, Image sprite, contract.IMap IMap) {
        super(position, sprite, IMap);
    }


    /**
     * Verify if this entity can roll.
     *
     * @return Returns true if the entity can roll.
     */
    @Override
    public boolean canRoll() {
        boolean boulderUnder = false;
        boolean diamondUnder = false;
        boolean leftBlocked = false;
        boolean rightBlocked = false;
        boolean bottomLeftBlocked = false;
        boolean bottomRightBlocked = false;

        List<IEntity> entities = new ArrayList<>(getMap().getEntities());

        for (IEntity entity :
                entities) {
            //  Player player = as(Player.class, entity);
            // if (player == null) {
            if (!bottomLeftBlocked)
                bottomLeftBlocked = hasOnBottomLeft(entity);
            if (!bottomRightBlocked)
                bottomRightBlocked = hasOnBottomRight(entity);
            //     }
            if (!leftBlocked)
                leftBlocked = hasOnLeft(entity);
            if (!rightBlocked)
                rightBlocked = hasOnRight(entity);
            if (as(IGravityable.class, entity) != null) {
                Gravityable gravit = (Gravityable) entity;
                if (this.hasUnder(gravit)) {
                    if (gravit.isFalling() || gravit.isRolling())
                        return false;
                    if (as(Boulder.class, gravit) != null)
                        boulderUnder = true;
                    if (as(Diamond.class, gravit) != null)
                        diamondUnder = true;
                }
            }

        }

        rollLeft = !(leftBlocked || bottomLeftBlocked);
        rollRight = !(rightBlocked || bottomRightBlocked);

        if ((leftBlocked && rightBlocked) || (bottomLeftBlocked && bottomRightBlocked) ||
                !(diamondUnder || boulderUnder))
            return false;
        else {
            if (as(Diamond.class, this) != null)
                return diamondUnder;
            else
                return true;
        }
    }


    /**
     * Makes roll at right or left this entity.
     */

    @Override
    public void roll() {

        if (this.isFalling() || this.isRolling()) {
            this.setRolling(false);
            return;
        }
        if (this.canRoll()) {
            if (!(rollRight || rollLeft)) {
                setRolling(false);
                return;
            }
            if (rollRight && rollLeft) {
                Random rand = new Random();
                boolean left = rand.nextBoolean();
                if (left) {
                    position.setGridCellX((position.getGridCellX() - 1));
                } else {
                    position.setGridCellX((position.getGridCellX() + 1));
                }
            } else if (!rollRight) {
                position.setGridCellX((position.getGridCellX() - 1));
            } else {
                position.setGridCellX((position.getGridCellX() + 1));
            }
            setRolling(true);
        } else setRolling(false);
    }

    /**
     * @return Returns rolling attribute.
     */
    @Override
    public boolean isRolling() {
        return this.rolling;
    }

    /**
     * @param rolling Sets rolling attribute.
     */
    public void setRolling(boolean rolling) {
        this.rolling = rolling;
    }

    /**
     * Verify if this entity can fall.
     *
     * @return Returns true if the entity can fall.
     */
    @Override
    public boolean canFall() {

        IPosition futurePosition = this.position.clone();
        List<IEntity> entities = new ArrayList<>(getMap().getEntities());

        futurePosition.setGridCellY(futurePosition.getGridCellY() + 1);

        for (IEntity entity : entities) {

            if (entity.getPosition().equals(futurePosition))
                if (entity.isColliding(this, futurePosition)) {

                    return false;

                }
        }
        return true;
    }

    /**
     * Makes fall this entity.
     */
    @Override
    public void fall() {

        if (this.canFall()) {
            position.setGridCellY((position.getGridCellY() + 1));
            setFalling(true);
        } else setFalling(false);
    }

    /**
     * @return Returns falling attribute.
     */
    @Override
    public boolean isFalling() {
        return this.falling;
    }

    /**
     * @param falling Sets falling attribute.
     */
    public void setFalling(boolean falling) {
        this.falling = falling;
    }
}
