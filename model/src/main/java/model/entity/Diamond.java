package model.entity;

import contract.IDestroyable;
import contract.IEntity;
import contract.IPosition;
import model.world.Position;

import java.awt.*;

import static utils.Utils.as;

public class Diamond extends Gravityable implements IDestroyable {

    /**
     *
     * @param position Its position in the map.
     * @param sprite Its sprite for the game.
     * @param IMap Its map.
     */
    public Diamond(Position position, Image sprite, contract.IMap IMap) {
        super(position, sprite, IMap);
    }

    /**
     * Checks whether this diamond and the entity passed to the method will collide.
     * @param entity The potentially colliding entity.
     * @param futurePosition Future position of the entity.
     * @return Returns true if the collision prevents movement.
     */
    @Override
    public boolean isColliding(IEntity entity, IPosition futurePosition) {
        if (this.getPosition().equals(futurePosition)) {
            if (as(Player.class, entity) != null) {
                getMap().decrementDiamondsCounter();
                this.destroy();
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
