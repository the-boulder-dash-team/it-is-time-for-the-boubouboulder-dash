package model.entity;

import contract.IDestroyable;
import contract.IEntity;
import contract.IMap;
import contract.IPosition;
import model.world.Position;

import java.awt.*;

import static utils.Utils.as;

public class Dirt extends Entity implements IDestroyable {

    /**
     *
     * @param position Its position in the map.
     * @param sprite Its sprite for the game.
     * @param IMap Its map.
     */
    public Dirt(Position position, Image sprite, IMap IMap) {
        super(position, sprite, IMap);
    }

    /**
     * Deletes this dirt.
     */
    @Override
    public void destroy() {
        this.getMap().getEntities().remove(this);
    }


    /**
     * Checks whether this dirt and the entity passed to the method will collide.
     * @param entity The potentially colliding entity.
     * @param futurePosition Future position of the entity.
     * @return Returns true if the collision prevents movement.
     */

    @Override
    public boolean isColliding(IEntity entity, IPosition futurePosition) {
        if (this.getPosition().equals(futurePosition)) {
            if (as(Player.class, entity) != null) {
                this.destroy();
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
