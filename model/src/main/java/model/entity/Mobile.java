package model.entity;

import contract.IMap;
import contract.IMobile;
import contract.IPosition;
import enums.Direction;
import model.world.Position;

import java.awt.*;

public abstract class Mobile extends Entity implements IMobile {

    /**
     *
     * @param position Its position in the map.
     * @param sprite Its sprite for the game.
     * @param IMap Its map.
     */
    public Mobile(Position position, Image sprite, IMap IMap) {
        super(position, sprite, IMap);

    }

    /**
     * Verify if this entity can move on her desired future position.
     * @param futurePosition Future position of this entity.
     * @return Returns true if the entity can move on on her desired future position.
     */

    @Override
    public boolean canMove(IPosition futurePosition) {

        if (futurePosition.getGridCellX() < 0 || futurePosition.getGridCellY() < 0 ||
                futurePosition.getGridCellX() >= 1600 / 32 || futurePosition.getGridCellY() >= (900 / 32) - 1)
            return false;

        for (int i = 0; i < getMap().getEntities().size(); i++) {
            if (getMap().getEntities().get(i).isColliding(this, futurePosition)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Moves this entity according to the result of canMove().
     * @param direction The direction where this entity wants to move.
     * @return Returns true if this entity moved.
     */
    @Override
    public boolean move(Direction direction) {

        IPosition futurePosition = this.getPosition().clone();
        switch (direction) {
            case LEFT:
                futurePosition.setGridCellX(this.getPosition().getGridCellX() - 1);
                if (canMove(futurePosition)) {
                    this.getPosition().setGridCellX((this.getPosition().getGridCellX() - 1));
                    return true;
                }

                break;
            case RIGHT:
                futurePosition.setGridCellX(this.getPosition().getGridCellX() + 1);
                if (canMove(futurePosition)) {
                    this.getPosition().setGridCellX((this.getPosition().getGridCellX() + 1));
                    return true;
                }
                break;
            case DOWN:
                futurePosition.setGridCellY(this.getPosition().getGridCellY() + 1);
                if (canMove(futurePosition)) {
                    this.getPosition().setGridCellY((this.getPosition().getGridCellY() + 1));
                    return true;
                }
                break;
            case UP:
                futurePosition.setGridCellY(this.getPosition().getGridCellY() - 1);
                if (canMove(futurePosition)) {
                    this.getPosition().setGridCellY((this.getPosition().getGridCellY() - 1));
                    return true;
                }
                break;
        }
        return false;
    }


    public void move() {

    }

}
