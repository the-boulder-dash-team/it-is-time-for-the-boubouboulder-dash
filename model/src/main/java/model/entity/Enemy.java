package model.entity;

import contract.IEnemy;
import contract.IEntity;
import contract.IMap;
import contract.IPosition;
import enums.Direction;
import model.world.Position;
import utils.SpriteManager;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static utils.Utils.as;

public abstract class Enemy extends Mobile implements IEnemy {

    private Direction chosenDirection;
    private Direction previousDirection;

    /**
     * @param position Its position in the map.
     * @param sprite   Its sprite for the game.
     * @param IMap     Its map.
     */
    public Enemy(Position position, Image sprite, IMap IMap) {
        super(position, sprite, IMap);
    }

    /**
     * Deletes this enemy and does explosion().
     */
    @Override
    public void destroy() {

        this.explosion();
        getMap().getEntities().remove(this);

    }

    /**
     * Checks whether this entity and the one passed to the method will collide.
     *
     * @param entity         The potentially colliding entity.
     * @param futurePosition Future position of the entity.
     * @return Returns true if the collision prevents movement.
     */
    @Override
    public boolean isColliding(IEntity entity, IPosition futurePosition) {
        if (this.getPosition().equals(futurePosition)) {
            if (as(Boulder.class, entity) != null || as(Diamond.class, entity) != null) {
                if (((Gravityable) entity).isFalling() || ((Gravityable) entity).isRolling()) {
                    this.destroy();
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Deletes destroyable entities two cells around this enemy.
     */
    protected void explosion() {

        List<IEntity> entities = new ArrayList<>(getMap().getEntities());

        for (IEntity entity : entities) {
                if (hasAbove(entity) || hasUnder(entity) || hasOnLeft(entity) || hasOnRight(entity) ||
                        hasOnBottomLeft(entity) || hasOnBottomRight(entity) || hasOnTopLeft(entity) ||
                        hasOnTopRight(entity)) {
                    Player player = as(Player.class, entity);
                    if(player == null)
                        entity.destroy();
                }
        }
    }


    /**
     * Replaces entities destroyed by the enemy with diamonds.
     */
    protected void putDiamonds() {
        for (int i = this.getPosition().getGridCellY() - 1; i <= this.getPosition().getGridCellY() + 1; i++) {
            for (int j = this.getPosition().getGridCellX() - 1; j <= this.getPosition().getGridCellX() + 1; j++) {
                getMap().getEntities().add(new Diamond(new Position(j, i),
                                                       SpriteManager.getInstance().getDiamondSprite(), getMap()));
            }
        }
    }

    /**
     * Moves randomly this enemy.
     */
    @Override
    public void move() {

        if (chosenDirection != null) {
            if (!move(chosenDirection)) {
                previousDirection = chosenDirection;
                chosenDirection = null;
            }
        } else {
            if (!(previousDirection == Direction.LEFT) && move(Direction.RIGHT)) {
                previousDirection = null;
                chosenDirection = Direction.RIGHT;
            } else if (!(previousDirection == Direction.UP) && move(Direction.DOWN)) {
                previousDirection = null;
                chosenDirection = Direction.DOWN;
            } else if (!(previousDirection == Direction.RIGHT) && move(Direction.LEFT)) {
                previousDirection = null;
                chosenDirection = Direction.LEFT;
            } else if (!(previousDirection == Direction.DOWN) && move(Direction.UP)) {
                previousDirection = null;
                chosenDirection = Direction.UP;
            } else {
                chosenDirection = null;
                previousDirection = null;
            }
        }

    }
}
