package model.entity;

import contract.*;
import model.world.Position;

import java.awt.*;
import java.util.Observable;

public abstract class Entity extends Observable implements IEntity, ICollideable, IDestroyable {

    protected IPosition position;
    private Image sprite;
    private IMap IMap;
    private boolean isDrawable = true;

    /**
     * @param position Its position in the map.
     * @param sprite   Its sprite for the game.
     * @param IMap     Its map.
     */
    public Entity(Position position, Image sprite, IMap IMap) {
        this.position = position;
        this.sprite = sprite;
        this.IMap = IMap;
    }

    public Entity(Position position, Image sprite) {
        this.position = position;
        this.sprite = sprite;
    }

    /**
     * @return Returns Position.
     */
    @Override
    public IPosition getPosition() {
        return position;
    }

    /**
     * @param position Sets Position.
     */
    @Override
    public void setPosition(IPosition position) {
        this.position = position;
    }

    /**
     * @return Returns false because it isn't the player.
     */
    @Override
    public boolean isPlayer() {
        return false;
    }


    /**
     * @return Returns Sprite.
     */
    @Override
    public Image getSprite() {
        return sprite;
    }

    /**
     * @param sprite Sets Sprite.
     */
    @Override
    public void setSprite(Image sprite) {
        this.sprite = sprite;
    }

    /**
     * @return Returns the Map.
     */
    @Override
    public IMap getMap() {
        return IMap;
    }

    /**
     * @param IMap Sets the Map.
     */
    @Override
    public void setMap(IMap IMap) {
        this.IMap = IMap;
    }

    /**
     * Checks whether this entity and the one passed to the method will collide.
     *
     * @param entity         The potentially colliding entity.
     * @param futurePosition Future position of the entity.
     * @return Returns true if the collision prevents movement.
     */
    @Override
    public boolean isColliding(IEntity entity, IPosition futurePosition) {

        return this.getPosition().equals(futurePosition);

    }

    public boolean hasAbove(IEntity entity){
        return entity.getPosition().getGridCellX() == this.getPosition().getGridCellX() &&
                entity.getPosition().getGridCellY() == this.getPosition().getGridCellY() -1;
    }

    public boolean hasUnder(IEntity entity) {
        return entity.getPosition().getGridCellX() == this.getPosition().getGridCellX() &&
                entity.getPosition().getGridCellY() == this.getPosition().getGridCellY() + 1;

    }

    public boolean hasOnLeft(IEntity entity) {
        return entity.getPosition().getGridCellX() == this.getPosition().getGridCellX() - 1 &&
                entity.getPosition().getGridCellY() == this.getPosition().getGridCellY();
    }

    public boolean hasOnRight(IEntity entity) {
        return entity.getPosition().getGridCellX() == this.getPosition().getGridCellX() + 1 &&
                entity.getPosition().getGridCellY() == this.getPosition().getGridCellY();
    }

    public boolean hasOnBottomRight(IEntity entity) {
        return entity.getPosition().getGridCellX() == this.getPosition().getGridCellX() + 1 &&
                entity.getPosition().getGridCellY() == this.getPosition().getGridCellY() + 1;
    }

    public boolean hasOnBottomLeft(IEntity entity) {
        return entity.getPosition().getGridCellX() == this.getPosition().getGridCellX() - 1 &&
                entity.getPosition().getGridCellY() == this.getPosition().getGridCellY() + 1;
    }

    public boolean hasOnTopLeft(IEntity entity) {
        return entity.getPosition().getGridCellX() == this.getPosition().getGridCellX() - 1 &&
                entity.getPosition().getGridCellY() == this.getPosition().getGridCellY() - 1;
    }
    public boolean hasOnTopRight(IEntity entity) {
        return entity.getPosition().getGridCellX() == this.getPosition().getGridCellX() + 1 &&
                entity.getPosition().getGridCellY() == this.getPosition().getGridCellY() - 1;
    }

    /**
     * @return Return isDrawable attribute.
     */
    public boolean isDrawable() {
        return isDrawable;
    }

    /**
     * @param drawable Sets isDrawable attribute.
     */
    public void setDrawable(boolean drawable) {
        isDrawable = drawable;
    }

    @Override
    public void destroy() {
        this.getMap().getEntities().remove(this);
    }
}
