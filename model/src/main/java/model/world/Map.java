package model.world;

import contract.*;

import javax.swing.*;
import java.util.List;

public class Map implements IMap {
    private IPlayer player;
    private List<IEntity> entities;
    private Timer time;
    private int diamondsCounter;
    private boolean cleaned = false;

    /**
     *
     * @param entities A list of entities present in the map.
     * @param time The limited time to finish the game.
     * @param diamondsCounter A counter of diamonds.
     */
    public Map(List<IEntity> entities, Timer time, int diamondsCounter) {
        this.entities = entities;
        this.time = time;
        this.diamondsCounter = diamondsCounter;

        for (IEntity entity :
                entities) {
            entity.setMap(this);
        }
    }

    /**
     *
     * @return Returns List of entities.
     */
    @Override
    public List<IEntity> getEntities() {
        return entities;
    }

    /**
     *
     * @param entities Sets List of entities.
     */
    @Override
    public void setEntities(List<IEntity> entities) {
        this.entities = entities;
    }

    /**
     *
     * @return Return time attribute.
     */
    @Override
    public Timer getTime() {
        return time;
    }

    /**
     *
     * @param time Sets time attribute.
     */
    @Override
    public void setTime(Timer time) {
        this.time = time;
    }

    /**
     *
     * @return Returns diamondsCounter attribute.
     */
    @Override
    public int getDiamondsCounter() {
        return diamondsCounter;
    }

    /**
     *
     * @param diamondsCounter Sets diamondsCounter attribute.
     */
    @Override
    public void setDiamondsCounter(int diamondsCounter) {
        this.diamondsCounter = diamondsCounter;
    }

    /**
     * Decreases counter of diamonds when a diamond is destroyed.
     */
    public void decrementDiamondsCounter() {
        this.diamondsCounter--;
        if (this.diamondsCounter < 0)
            this.diamondsCounter = 0;

    }

    @Override
    public IPlayer getPlayer() {
        for (IEntity entity :
                entities) {
            if (entity.isPlayer())
                return (IPlayer) entity;
        }
        return null;
    }

    @Override
    public void setPlayer(IPlayer player) {
        this.player = player;
    }

    public boolean isCleaned() {
        return cleaned;
    }

    public void setCleaned(boolean cleaned) {
        this.cleaned = cleaned;
    }


}
