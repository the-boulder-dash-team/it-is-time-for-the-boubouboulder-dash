package model.world;

import contract.IPosition;

/**
 * A class that is used to represent and manipulate a position.
 */
public class Position implements IPosition, Cloneable {
    /**
     * The x axis coordinate.
     */
    private int x;
    /**
     * The y axis coordinate.
     */
    private int y;

    private final int gridSize = 32 ;

    /**
     *
     * @param x The x axis in number of cells coordinate.
     * @param y The y axis in number of cells coordinate.
     */
    public Position(int x, int y) {
        this.x = (gridSize *x);
        this.y = (gridSize *y);
    }

    /**
     * The x axis coordinate in pixel getter.
     *
     * @return Returns the x axis coordinate in pixel.
     */
    public int getX() { return x; }

    /**
     * The x axis coordinate in pixel setter.
     *
     * @param x The new x axis coordinate in pixel.
     */
    public void setX(int x) { this.x = x; }

    /**
     * The y axis coordinate in pixel getter.
     *
     * @return Returns the y axis coordinate in pixel.
     */
    public int getY() {
        return y;
    }

    /**
     * The y axis coordinate in pixel setter.
     *
     * @param y The new y axis coordinate in pixel.
     */
    public void setY(int y) { this.y = y; }


    /**
     * Allows us to have the conversion of x axis in number of cells.
     * @return Returns the conversion of x axis in number of cells.
     */
    public int getGridCellX (){ return this.x/ gridSize; }

    /**
     * Allows us to have the conversion of y axis in number of cells.
     * @return Returns the conversion of y axis in number of cells.
     */
    public int getGridCellY (){ return this.y/ gridSize; }

    /**
     * Allows us to give the number of cells for x axis and converts it in pixel.
     */
    @Override
    public void setGridCellX(int x) {
        this.x = gridSize * x;
    }

    /**
     * Allows us to give the number of cells for y axis and converts it in pixel.
     */
    @Override
    public void setGridCellY(int y) {
        this.y = gridSize * y;
    }

    /**
     * Makes a clone of Position.
     * @return Returns a clone.
     */
    public Position clone(){
        try {
            return (Position)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Checks whether two objects are equals.
     * @param obj A position.
     * @return Returns true if the two positions are equals.
     */
    @Override
    public boolean equals(Object obj) {
        Position toTest = (Position) obj;
        return toTest.getGridCellX() == this.getGridCellX() && toTest.getGridCellY() == this.getGridCellY();
    }
}