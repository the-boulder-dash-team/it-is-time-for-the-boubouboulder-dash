package model.dao;

import contract.IEntity;
import model.entity.*;
import model.world.Position;
import utils.SpriteManager;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EntityDAO {
    private DAOFactory dao;


    public EntityDAO(DAOFactory dao) {
        this.dao = dao;
    }

    public List<IEntity> getMapEntities(int map) {
        List<IEntity> entities = new ArrayList<>();

        Connection con = this.dao.getConnection();

        if (con != null) {
            String sql = "{call EntitiesByMap(?)}";
            try {
                CallableStatement statement = con.prepareCall(sql);
                statement.setInt(1, map);
                statement.execute();
                ResultSet set = statement.getResultSet();
                System.out.println(set.toString());
                while (set.next()) {
                    TypeEntity type = TypeEntity.values()[set.getInt("entity_type")];
                    Position position = new Position(set.getInt("x"), set.getInt("y"));
                    switch (type) {
                        case BreakableWally:
                            entities.add(new BreakableWall(position,
                                                           SpriteManager.getInstance().getBreakableWallSprite(), null));
                            break;
                        case BoundingWally:
                            entities.add(new BoundingWall(position,
                                                          SpriteManager.getInstance().getBoundingWallSprite(), null));
                            break;
                        case Dirty:
                            entities.add(new Dirt(position,
                                                  SpriteManager.getInstance().getDirtSprite(), null));
                            break;
                        case Backgroundy:
                            break;
                        case Bouldery:
                            entities.add(new Boulder(position,
                                                     SpriteManager.getInstance().getBoulderSprite(), null));
                            break;
                        case Diamondy:
                            entities.add(new Diamond(position,
                                                     SpriteManager.getInstance().getDiamondSprite(), null));
                            break;
                        case EndBlocky:
                            entities.add(new EndBlock(position,
                                                      SpriteManager.getInstance().getEndBlockSprite(), null));
                            break;
                        case Butterflyy:
                            entities.add(new Butterfly(position,
                                                       SpriteManager.getInstance().getEnemySprites().get("enemy"),
                                                       null));
                            break;
                        case Playery:
                            entities.add(new Player(position, true,
                                                    SpriteManager.getInstance().getPlayerSprites().get("down"), null));
                            break;
                        case FasteryEnemyy:
                            entities.add(new FastEnnemy(position, SpriteManager.getInstance().getEnemySprites().get(
                                    "enemy"), null));
                    }

                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return entities;
    }
}
