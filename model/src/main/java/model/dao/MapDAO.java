package model.dao;

import contract.IEntity;
import model.world.Map;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MapDAO {
    private DAOFactory dao;


    public MapDAO(DAOFactory dao) {
        this.dao = dao;
    }

    public Map getMap(int mapID) {
        Map map = null;
        List<IEntity> entities = this.dao.getEntityDAO().getMapEntities(mapID);
        Connection con = this.dao.getConnection();

        if (con != null) {
            String sql = "{call MapById(?)}";
            CallableStatement statement = null;
            try {
                statement = con.prepareCall(sql);
                statement.setInt(1, mapID);
                statement.execute();
                ResultSet set = statement.getResultSet();
                set.first();
                map = new Map(entities, null, set.getInt("diamonds"));
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return map;
    }
}
