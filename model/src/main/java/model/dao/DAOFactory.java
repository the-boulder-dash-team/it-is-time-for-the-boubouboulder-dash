package model.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DAOFactory {
    private static final String propertiesFile = "dao.properties";
    private static final String propertyUrl = "url";
    private static final String propertyDriver = "driver";
    private static final String propertyUser = "user";
    private static final String propertyPassword = "password";

    private static DAOFactory daoFactory = null;

    private String url;
    private String user;
    private String password;

    private DAOFactory(String url, String user, String password){
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public static DAOFactory getInstance() throws DAOConfigurationException{
        if(daoFactory == null){
            Properties properties = new Properties();
            String url;
            String user;
            String driver;
            String password;

            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream propertiesFile = classLoader.getResourceAsStream(DAOFactory.propertiesFile);

            if( propertiesFile == null){
                throw new DAOConfigurationException("The properties file " + DAOFactory.propertiesFile + " was not found.");
            }

            try{
                properties.load(propertiesFile);
                url = properties.getProperty(propertyUrl);
                driver = properties.getProperty(propertyDriver);
                user = properties.getProperty(propertyUser);
                password = properties.getProperty(propertyPassword);
            }catch( IOException e){
                throw new DAOConfigurationException("We could not load the properties file " + DAOFactory.propertiesFile + ".", e);
            }

            try{
                Class.forName(driver);
            }catch(ClassNotFoundException e){
                throw new DAOConfigurationException("The driver was not found.", e);
            }

            DAOFactory.daoFactory = new DAOFactory(url, user, password);

        }
        return DAOFactory.daoFactory;
    }

    public Connection getConnection(){
        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public EntityDAO getEntityDAO(){
        return new EntityDAO(this);
    }

    public MapDAO getMapDAO(){
        return new MapDAO(this);
    }

}
