package view;

import contract.IEntity;
import utils.SpriteManager;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

class ViewPanel extends JPanel implements Observer {

    private static final long serialVersionUID = -998294702363713521L;
    private BufferedImage background;
    private BufferedImage gameImage;
    private ViewFrame viewFrame;

    /**
     *
     * @param viewFrame Game's frame.
     */
    public ViewPanel(final ViewFrame viewFrame) {
        this.setViewFrame(viewFrame);
        this.viewFrame.getGameController().getObservable().addObserver(this);
        background = new BufferedImage(viewFrame.getWidth(), viewFrame.getHeight(),
                                       BufferedImage.TYPE_INT_RGB);

        Graphics2D g = background.createGraphics();
        for (int i = 0; i < viewFrame.getHeight() / 32 + 1; i++) {
            for (int j = 0; j < viewFrame.getWidth() / 32 + 1; j++) {
                g.drawImage(SpriteManager.getInstance().getBackgroundSprite(), j * 32, i * 32, 32, 32, this);
            }
        }
        gameImage = new BufferedImage(1600, 900, BufferedImage.TYPE_INT_RGB);
        this.update(null, null);
    }

    /**
     * Gets the view frame.
     *
     * @return the view frame
     */
    private ViewFrame getViewFrame() {
        return this.viewFrame;
    }

    /**
     * Sets the view frame.
     *
     * @param viewFrame the new view frame
     */
    private void setViewFrame(final ViewFrame viewFrame) {
        this.viewFrame = viewFrame;
    }


    public void update(final Observable arg0, final Object arg1) {
        this.viewFrame.setTitle("Bouldery Dashy - Remaining diamonds: " + this.viewFrame.getMap().getDiamondsCounter());
        List<IEntity> entities = new ArrayList<>(viewFrame.getMap().getEntities());
        Graphics2D g2d = (Graphics2D) gameImage.getGraphics();
        g2d.clearRect(0, 0, this.getWidth(), this.getHeight());

        if (arg1 != null) {
            boolean lost = (boolean) arg1;
            if(lost) {
                g2d.setBackground(Color.BLACK);
                g2d.setColor(Color.RED);
                g2d.setFont(new Font("Arial", Font.BOLD, 48));
                g2d.drawString("YOU DIEDY", 650, 450);
            }else
            {
                g2d.setBackground(Color.WHITE);
                g2d.setColor(Color.GREEN);
                g2d.setFont(new Font("Arial", Font.BOLD, 48));
                g2d.drawString("YOU WONY", 650, 450);
            }
        } else {
            g2d.drawImage(background, 0, 0, this);
            for (IEntity e : entities) {
                if (e.isDrawable())
                    g2d.drawImage(e.getSprite(), e.getPosition().getX(), e.getPosition().getY(), 32, 32, this);
            }
        }

        this.repaint();
    }


    @Override
    protected void paintComponent(final Graphics g) {

        g.drawImage(gameImage, 0, 0, this);

    }
}
