package view;

import contract.IController;
import contract.IGameController;
import contract.IMap;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ViewFrame extends JFrame implements KeyListener {

    private IMap map;
    private IController controller;
    private IGameController game;

    /**
     *
     * @param map Game's map.
     */
    public ViewFrame(IMap map) {
        this.map = map;
        this.build();
    }


    /**
     *
     * @param c Controller for the game.
     * @param map Game's map.
     */
    public ViewFrame(IGameController c, IMap map) {
        this.game = c;
        this.map = map;
        this.build();
    }

    /**
     * Builds the window.
     */
    private void build() {
        this.setVisible(false);
        this.setTitle("Bouldery Dashy - Remaining diamonds: " + map.getDiamondsCounter());
        this.setSize(1600, 900);
        this.setResizable(false);
        this.addKeyListener(this);
        this.setLocationRelativeTo(null);
        this.setContentPane(new ViewPanel(this));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     *
     * @return Returns Controller.
     */
    public IController getController() {
        return this.controller;
    }

    /**
     *
     * @param controller Sets Controller.
     */
    public void setController(IController controller) {
        this.controller = controller;
    }


    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * Recovers key pressed by the user.
     * @param e The key pressed.
     */
    @Override
    public void keyPressed(KeyEvent e) {

        this.game.handleKeyEvent(e);
    }

    /**
     * Recovers key released by the user.
     * @param e The key released.
     */
    @Override
    public void keyReleased(KeyEvent e) {
        this.game.handleKeyEvent(e);
    }

    public IGameController getGameController() {
        return game;
    }

    public void setGameController(IGameController game) {
        this.game = game;
    }

    public IMap getMap() {
        return map;
    }

    public void setMap(IMap map) {
        this.map = map;
    }
}

