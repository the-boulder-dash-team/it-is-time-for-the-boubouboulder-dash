package view;

import contract.*;

import javax.swing.*;

public class View implements IView, Runnable {


    private ViewFrame viewFrame;

    /**
     *
     * @param map Game's map.
     */
    public View(IMap map) {
        this.viewFrame = new ViewFrame(map);
        SwingUtilities.invokeLater(this);
    }

    /**
     *
     * @param map Game's map.
     * @param game Controller for the game.
     */
    public View(IMap map, IGameController game) {
        this.viewFrame = new ViewFrame(game, map);
        SwingUtilities.invokeLater(this);
    }

    /**
     *
     * @return Returns ViewFrame.
     */
    public ViewFrame getViewFrame() {
        return viewFrame;
    }

    /**
     *
     * @param viewFrame Sets ViewFrame.
     */
    public void setViewFrame(ViewFrame viewFrame) {
        this.viewFrame = viewFrame;
    }

    /**
     * Makes appear the window
     */
    @Override
    public void show() {
        this.viewFrame.setVisible(true);
    }

    /**
     * Makes disappear the window
     */
    public void hide() {
        this.viewFrame.setVisible(false);
    }

    /**
     * Runs show().
     */
    @Override
    public void run() {
        this.show();
    }

    /**
     *
     * @param controller Sets Controller.
     */
    public void setController(IController controller) {
        this.viewFrame.setController(controller);
    }

    /**
     *
     * @param controller Sets GameEngine.
     */
    public void setGameController(IGameController controller){
        this.viewFrame.setGameController(controller);
    }
}
